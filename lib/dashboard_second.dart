import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';
import 'package:foomart/Components/ResturantCardVertical.dart';
import 'package:foomart/Constants.dart';

import 'Components/SearchAndLocationAppBar.dart';

class DashboardSecond extends StatefulWidget {
  @override
  _DashboardSecondState createState() => _DashboardSecondState();
}

class _DashboardSecondState extends State<DashboardSecond>
    with TickerProviderStateMixin {
  TabController _tabController;
  GlobalKey _tabKey;
  @override
  void initState() {
    // TODO: implement initState
    _tabController = TabController(length: 3, vsync: this, initialIndex: 0);
    _tabKey = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return SafeArea(
        child: Scaffold(
      backgroundColor: Color(0xFFF9F9F9),
      body: SingleChildScrollView(
        child: Container(
          // height: _size.height,
          width: _size.width,
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SearchAndLocationAppBar(),
              SizedBox(
                height: 20,
              ),
              Placeholder(
                fallbackHeight: _size.height / 2 * _ratio,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Category",
                style: AppConstants.bodyFont
                    .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
              ),
              SizedBox(
                height: 10.0,
              ),
              Card(
                elevation: 8,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                clipBehavior: Clip.hardEdge,
                child: Container(
                  color: AppConstants.appGrey,
                  child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    mainAxisSpacing: 1,
                    crossAxisSpacing: 1,
                    children: [
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Restaurant",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Grocery",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Fruits & Vegetables",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Meat & Fish",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Pharmacy",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: SvgPicture.asset(
                              "assets/icons/restaurant.svg",
                              fit: BoxFit.contain,
                            )),
                            SizedBox(
                              height: 10.0,
                            ),
                            Flexible(
                              child: Text(
                                "Package & Courier",
                                style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 1,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TabBar(
                key: _tabKey,
                controller: _tabController,
                labelPadding: EdgeInsets.zero,
                indicatorWeight: 0.1,
                labelStyle: AppConstants.bodyFont,
                onTap: (index) {
                  print(_tabController.animation.value);
                },
                tabs: [
                  Container(
                    decoration: BoxDecoration(
                        color: AppConstants.appGreen,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10))),
                    width: double.infinity,
                    child: Tab(
                      text: "Best Offers",
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    color: AppConstants.appGreen,
                    child: Tab(
                      child: Container(
                        child: Text("Popular"),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: AppConstants.appGreen,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10))),
                    width: double.infinity,
                    child: Tab(
                      child: Container(
                        child: Text("Top Rated"),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0,),
              Container(
                height: 400,
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 80,
                          child: Card(
                            elevation: 10,
                            child: Row(
                              children: [
                                Flexible(flex:2,child: Placeholder()),
                                Spacer(),
                                Flexible(flex:5,child: Text("UP TO 50% \nMIN ORDER \u20b9 200.", style: AppConstants.headingFont.apply(letterSpacingFactor: 1.2), textAlign: TextAlign.center,))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 400,
                      width: double.infinity,
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 600,
                      width: double.infinity,
                      color: Colors.blueGrey,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CustomBottomNavigation(),
    ));
  }
}
