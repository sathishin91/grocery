import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';
import 'package:foomart/Components/SearchAndLocationAppBar.dart';

import '../Constants.dart';

class SortFilter extends StatefulWidget {
  @override
  _SortFilterState createState() => _SortFilterState();
}

class _SortFilterState extends State<SortFilter> {
  double _price = 0;
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(

      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {},
        ),
        title: Text(
          "Order History",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          height: _size.height,
          margin: EdgeInsets.only(left: 16,right: 16,top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SearchAndLocationAppBar(),
              SizedBox(height: 70.0,),
              Container(margin:EdgeInsets.symmetric(vertical: 20),child: Text("Sort By", style: AppConstants.bodyFont,)),
              Row(
                children: [
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                ],
              ),
              Container(margin:EdgeInsets.symmetric(vertical: 15.0),child: Text("Filter By", style: AppConstants.bodyFont,)),
              Row(
                children: [
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Container(
                      width: double.infinity,
                      child: TextButton(
                        child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                        style: TextButton.styleFrom(
                          backgroundColor:AppConstants.appGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),

                ],
              ),
              Container(margin:EdgeInsets.symmetric(vertical: 15.0),child: Text("Price", style: AppConstants.bodyFont,)),
              Slider(
                value: _price,
                onChanged: (newPrice){
                  setState(() {
                    _price = newPrice;
                  });
                },
                min: 0,
                max: 300,

              ),
              SizedBox(height: 60,),
              BlockButton(btnLabel: "Apply",),
              Spacer()

            ],
          ),
        ),
      ),
    );
  }
}
