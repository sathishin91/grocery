import 'package:flutter/material.dart';
import 'package:foomart/Constants.dart';

import 'BlockButton.dart';

class LocationCheck extends StatefulWidget {
  @override
  _LocationCheckState createState() => _LocationCheckState();
}

class _LocationCheckState extends State<LocationCheck> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;

    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
          child: Container(
        height: _size.height,
        width: _size.width,
        margin: EdgeInsets.symmetric(horizontal: 52 * _ratio),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 336 * _ratio),
            RichText(
              text: TextSpan(
                text: "Hi Sathish Welcome To ",
                children: [
                  TextSpan(text:"FooMart", style: TextStyle(color: AppConstants.appGreen)),
                ],
                style: AppConstants.bodyFont.apply(fontSizeFactor: 3, fontWeightDelta: 2).merge(TextStyle(height: 1.2)),
              ),

              softWrap: true,
            ),
            SizedBox(
              height: 150 * _ratio,
            ),
            Text(
              "Please turn on your GPS to find out better restaurants suggestions near you",
              style: AppConstants.bodyFont,
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 75 * _ratio,
            ),
            SizedBox(
              height: 70 * _ratio,
            ),
            BlockButton(
              btnLabel: "VERIFY",
              btnFunction: () {},
            )
          ],
        ),
      )),
    ));
  }
}
