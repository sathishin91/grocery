import 'package:flutter/material.dart';

import '../Constants.dart';

class AccountEdit extends StatefulWidget {
  @override
  _AccountEditState createState() => _AccountEditState();
}

class _AccountEditState extends State<AccountEdit> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {Navigator.pop(context);},
        ),
        title: Text(
          "Address Book",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: SingleChildScrollView(
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Your Name",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "Sayed Akhtar",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Your Email",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "example@gmail.com",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Your Mobile Number",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "+91 7602121828",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20.0),
                    width: double.infinity,
                    child: Center(
                      child: OutlinedButton(
                        onPressed: () {},
                        child: Text("Edit Profile"),
                        style: OutlinedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: 80.0),
                            side: BorderSide(color: AppConstants.appGreen)),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
