import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../Constants.dart';
import 'Pill.dart';

class RestaurantCardHorizontalWithCounter extends StatefulWidget {
  @override
  _RestaurantCardHorizontalWithCounterState createState() => _RestaurantCardHorizontalWithCounterState();
}

class _RestaurantCardHorizontalWithCounterState extends State<RestaurantCardHorizontalWithCounter> {
  int _itemCount = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = 1;
  }
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return Container(
      margin: EdgeInsets.only(bottom: 7.0),
      width: 280.0,
      height: 300.0,
      // constraints: BoxConstraints(maxHeight: 220.0, minHeight: 200.0, maxWidth: 200, minWidth: 200.0),
      child: Card(
        elevation: 10,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        shadowColor: Colors.black.withOpacity(0.22),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Flexible(
                child: Placeholder(
                  color: Colors.blueGrey,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Tandoori Chicken",
                      style: AppConstants.bodyFont
                          .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                    ),
                    Spacer(),
                    Pill(
                        label: Text(
                          "Full",
                          style: AppConstants.bodyFont.apply(
                              fontSizeFactor: 0.6, color: Colors.white),
                        ),
                        gradient: LinearGradient(colors: [
                          AppConstants.appGreen,
                          AppConstants.appGreen
                        ])),
                    Pill(
                        label: Text(
                          "Half",
                          style: AppConstants.bodyFont.apply(
                              fontSizeFactor: 0.6, color: Colors.white),
                        ),
                        gradient: LinearGradient(colors: [
                          AppConstants.appGrey,
                          AppConstants.appGrey,
                        ],),),

                  ],
                ),
              ),
              Row(
                children: [
                  Pill(
                    label: Text(
                      "\u20B9 200",
                      style: AppConstants.bodyFont.apply(
                          fontSizeFactor: 0.7,
                          color: AppConstants.appGreen),
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: 120,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppConstants.appGreen),
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            if(_itemCount >0){
                              setState(() {
                                _itemCount--;
                              });
                            }
                          },
                          child: Container(
                              padding: EdgeInsets.all(5.0),

                              child: Icon(
                                Icons.remove,
                                color: AppConstants.appGreen,
                              )),
                        ),
                        Container(
                            height: 17.0 + 16,
                            width: 24.0 + 16,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            child:Center(child: Text("${_itemCount}"))
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _itemCount++;
                            });
                          },
                          child: Container(
                              padding: EdgeInsets.all(5.0),

                              child: Icon(
                                Icons.add,
                                color: AppConstants.appGreen,
                              )),
                        ),
                      ],
                    ),
                  ),


                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
