import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';

import '../Constants.dart';

class AddEditAddress extends StatefulWidget {
  @override
  _AddEditAddressState createState() => _AddEditAddressState();
}

class _AddEditAddressState extends State<AddEditAddress> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Address Book",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: SingleChildScrollView(
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Door No & Building & Apartment address.",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "10th road, Crossing Road",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Street Name",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "10th road, Crossing Road",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "City",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "10th road, Crossing Road",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Landmark",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "10th road, Crossing Road",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Pincode",
                        style: AppConstants.headingFont
                            .merge(TextStyle(fontWeight: FontWeight.w400)),
                      )),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "10th road, Crossing Road",
                      hintStyle: AppConstants.bodyFont.apply(fontSizeFactor: 0.8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: AppConstants.appGreen),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20.0),
                    width: double.infinity,
                    child: Center(
                      child: OutlinedButton(
                        onPressed: () {},
                        child: Text("Save"),
                        style: OutlinedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: 80.0),
                            side: BorderSide(color: AppConstants.appGreen)),
                      ),
                    ),
                  ),
                  SizedBox(height: 40.0,),
                  BlockButton(
                    btnLabel: "set automatic location".toUpperCase(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: CustomBottomNavigation(),
    );
  }
}
