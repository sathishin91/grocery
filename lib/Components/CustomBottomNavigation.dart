import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../Constants.dart';

class CustomBottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
      color: Color(0xFFf6f6f6),
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      height: 60,
      child: Stack(
        fit: StackFit.expand,
        clipBehavior: Clip.none,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              width: _size.width / 2.5 - 16 * 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () => {Navigator.pushReplacementNamed(context, '/home')},
                    child: SvgPicture.asset('assets/icons/home.svg', height: 30, width:30,fit: BoxFit.contain,),
                  ),
                  GestureDetector(
                    onTap: () => {Navigator.pushReplacementNamed(context, '/favourites')},
                    child: SvgPicture.asset('assets/icons/favourite.svg', height: 30, width:30,fit: BoxFit.contain,),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: -20,
            left: _size.width / 2 - 45,
            child: Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1000),
                color: AppConstants.appGreen,
              ),
              child: GestureDetector(
                onTap: () => {Navigator.pushReplacementNamed(context, '/checkout')},
                child: Icon(
                  Icons.shopping_cart_rounded,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              width: _size.width / 2.5 - 16 * 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () => {},
                    child: SvgPicture.asset('assets/icons/bell.svg', height: 30, width:30,fit: BoxFit.contain,),
                  ),
                  GestureDetector(
                    onTap: () =>
                        {Navigator.pushNamed(context, '/accounts_page')},
                    child: SvgPicture.asset('assets/icons/user.svg', height: 30, width:30,fit: BoxFit.contain,),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
