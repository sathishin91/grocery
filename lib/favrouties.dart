import 'package:flutter/material.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';

import 'Components/Pill.dart';
import 'Constants.dart';

class Favourites extends StatefulWidget {
  @override
  _FavouritesState createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {Navigator.pop(context);},
        ),
        title: Text(
          "Address Book",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            ResturantCard(_size),
            ResturantCard(_size),
          ],
        ),
      ),
      bottomNavigationBar: CustomBottomNavigation(),
    );
  }
  Widget ResturantCard(Size _size){
    return Container(
      margin: EdgeInsets.only(bottom: 7.0),
      width: _size.width,
      constraints: BoxConstraints(maxHeight: 160.0, minHeight: 120.0),
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)),
        shadowColor: Colors.black.withOpacity(0.22),
        child: Row(
          children: [
            Flexible(
              flex: 2,
              child: Placeholder(
                color: Colors.blueGrey,
              ),
            ),
            Flexible(
              flex: 4,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Imperial Restaurant".toUpperCase(),
                      style: AppConstants.bodyFont.apply(
                          fontWeightDelta: 1, color: Color(0xff3E3F68)),
                    ),
                    Text(
                      "Indian, Arabic, Chineese",
                      style:
                      AppConstants.bodyFont.apply(fontSizeFactor: 0.7),
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Text(
                          "Closing 4pm",
                          style: AppConstants.bodyFont
                              .apply(color: AppConstants.appGreen),
                        ),
                        Spacer(),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                        ),
                        Text(
                          " 4.9",
                          style: AppConstants.bodyFont.apply(
                            fontSizeFactor: 1.2,
                            color: Colors.yellow,
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Icon(
                          Icons.location_pin,
                          color: AppConstants.appGreen,
                        ),
                        Text(
                          " Millers's Road",
                          style: AppConstants.bodyFont.apply(
                            fontSizeFactor: 0.6,
                          ),
                        ),
                        Spacer(),
                        Text(
                          "0.5 kms",
                          style: AppConstants.bodyFont
                              .apply(fontSizeFactor: 0.6),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
