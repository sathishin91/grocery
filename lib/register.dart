import 'package:flutter/material.dart';
import 'package:foomart/Components/BlockButton.dart';
import 'package:foomart/Constants.dart';
import 'package:google_fonts/google_fonts.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  FocusNode _mobileTextFocusNode = new FocusNode();
  FocusNode _otpTextFocusNode = new FocusNode();

  TextEditingController _mobileTextController = new TextEditingController();
  TextEditingController _otpTextController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;

    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Container(
        height: _size.height,
        width: _size.width,
        margin: EdgeInsets.symmetric(horizontal: 52 * _ratio),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 130 * _ratio),
            Image.asset("assets/logo.png", height: 195),
            SizedBox(
              height: 80 * _ratio,
            ),
            Text(
              "Let's Discover\nThousands of restaurants",
              style: AppConstants.bodyFont,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 75 * _ratio,
            ),
            TextFormField(
              controller: _mobileTextController,
              focusNode: _mobileTextFocusNode,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Your Mobile Number",
                hintStyle: AppConstants.bodyFont,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide.none),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide.none),
                fillColor: AppConstants.appGreen.withOpacity(0.4),
                filled: true,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              controller: _otpTextController,
              focusNode: _otpTextFocusNode,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Your OTP",
                hintStyle: AppConstants.bodyFont,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide.none),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide.none),
                fillColor: AppConstants.appGreen.withOpacity(0.4),
                filled: true,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              width: _size.width - (52 * _ratio) * 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Text(
                      "Resend OTP",
                      style: GoogleFonts.josefinSans(
                        fontSize: 16,
                        color: AppConstants.appGreen,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 70 * _ratio,
            ),
            BlockButton(
              btnLabel: "VERIFY",
              btnFunction: () {},
            )
          ],
        ),
      )),
    ));
  }
}
