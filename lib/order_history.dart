import 'package:flutter/material.dart';

import 'Constants.dart';

class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {},
        ),
        title: Text(
          "Order History",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              width: _size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
              child: Card(
                elevation: 8,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15,),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Imperial Restaurant".toUpperCase(),
                        style: AppConstants.bodyFont
                            .apply(fontWeightDelta: 1, color: Color(0xff3E3F68)),
                      ),
                      Text(
                        "Miller's Road, 0.5 Kms",
                        style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7),
                      ),
                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Tandoori Chicken",
                                  style: AppConstants.bodyFont.apply(
                                      fontSizeFactor: 0.6,
                                      color: AppConstants.appGreen),
                                ),
                                Text(
                                  "Tandoori Chicken",
                                  style: AppConstants.bodyFont.apply(
                                      fontSizeFactor: 0.6,
                                      color: AppConstants.appGreen),
                                ),

                              ],
                            ),
                          ),
                          Expanded(flex: 1, child: Text("\u20B9 75", textAlign: TextAlign.right,))
                        ],
                      ),
                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Flexible(
                            child: Container(
                              width: double.infinity,
                              child: TextButton(
                                child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                                style: TextButton.styleFrom(
                                  backgroundColor:AppConstants.appGreen,
                                ),
                                onPressed: (){},
                              ),
                            ),
                          ),
                          SizedBox(width: 10,),
                          Flexible(
                            child: Container(
                              width: double.infinity,
                              child: TextButton(
                                child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                                style: TextButton.styleFrom(
                                  backgroundColor:AppConstants.appGreen,
                                ),
                                onPressed: (){},
                              ),
                            ),
                          ),
                          SizedBox(width: 10,),
                          Flexible(
                            child: Container(
                              width: double.infinity,
                              child: TextButton(
                                child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: Colors.white),),
                                style: TextButton.styleFrom(
                                  backgroundColor:AppConstants.appGreen,
                                ),
                                onPressed: (){},
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
