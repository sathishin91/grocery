import 'package:flutter/material.dart';
import 'package:foomart/Components/CustomListTile.dart';

import 'Constants.dart';

class ViewOrder extends StatefulWidget {
  @override
  _ViewOrderState createState() => _ViewOrderState();
}

class _ViewOrderState extends State<ViewOrder> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {},
        ),
        title: Text(
          "Order History",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                transform: Matrix4.translationValues(0, 5, -10),
                child: Card(
            elevation: 10,
            shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), topLeft: Radius.circular(20))),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16.0),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Stack(
                            children: [
                              Positioned(
                                top: 30,
                                left: 12,
                                child: Container(
                                  height: 20,
                                  width: 1,
                                  color: AppConstants.appGrey,
                                ),
                              ),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Row(
                                  children: [
                                    Icon(Icons.location_pin, color: AppConstants.appGreen,),
                                    SizedBox(width: 20,),
                                    RichText(
                                      text: TextSpan(
                                          children: [
                                            TextSpan(text:"Imperial Restaurant".toUpperCase(), style: AppConstants.bodyFont.apply(color: Colors.black)),
                                            TextSpan(text:"\nMiller's Road", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.6))
                                          ]
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 80,),
                              Positioned(
                                bottom: 0,
                                child: Row(
                                  children: [
                                    Icon(Icons.location_pin, color: AppConstants.appGreen,),
                                    SizedBox(width: 20,),
                                    RichText(
                                      text: TextSpan(
                                          children: [
                                            TextSpan(text:"Home".toUpperCase(), style: AppConstants.bodyFont.apply(color: Colors.black)),
                                            TextSpan(text:"\nNo.10,9th Main, Vasanth Nagar, Bangalore.", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.6))
                                          ]
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),

                          Divider(),
                          Row(
                            children: [
                              Text("Order Date: 20-0-2021 \nDelivery Status : Delivered "),
                              Spacer(),
                              Text("Order ID: 31321"),
                            ],
                          ),
                          Divider(),
                          Text("BILL DETAILS", style: TextStyle(fontWeight: FontWeight.w700),)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                color: Colors.white,
                child: Container(
                  transform: Matrix4.translationValues(0, 0, 10),
                  color: AppConstants.appGreen.withOpacity(0.3),
                  child:Column(
                    children: [
                      Divider(),
                      CustomListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        title: Text("Tandoori Chicken X1 Half"),
                        trailing: Text("\u20b9 190", style: TextStyle(fontWeight: FontWeight.w700),),
                      ),
                      Divider(),
                      CustomListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        title: Text("Tandoori Chicken X1 Half"),
                        trailing: Text("\u20b9 190", style: TextStyle(fontWeight: FontWeight.w700),),
                      ),
                      Divider(),
                      CustomListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        title: Text("Tandoori Chicken X1 Half"),
                        trailing: Text("\u20b9 190", style: TextStyle(fontWeight: FontWeight.w700),),
                      ),
                      Divider(),
                      CustomListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        title: Text("Tandoori Chicken X1 Half"),
                        trailing: Text("\u20b9 190", style: TextStyle(fontWeight: FontWeight.w700),),
                      ),
                      Divider(),
                      CustomListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        title: Text("Tandoori Chicken X1 Half"),
                        trailing: Text("\u20b9 190", style: TextStyle(fontWeight: FontWeight.w700),),
                      ),
                      Divider(),
                    ],
                  )
                ),
              ),
              Container(
                transform: Matrix4.translationValues(0, -5, -10),
                child: Card(
                  shadowColor: Colors.black.withOpacity(0.2),
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15))),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Sub Total",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 68",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Delivery Cost",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 2",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("Discount",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 8",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(horizontal: 0),
                          title: Text("GST",
                              style: AppConstants.bodyFont.apply(
                                  fontSizeFactor: 0.7,
                                  fontWeightDelta: 3,
                                  color: Colors.black)),
                          trailing: Text(
                            "\u20B9 2",
                            style: AppConstants.headingFont,
                          ),
                        ),
                        Divider(),
                        CustomListTile(
                          contentPadding: EdgeInsets.symmetric(vertical: 8),
                          title: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(text:"Paid Total",
                                    style: AppConstants.bodyFont.apply(
                                        fontSizeFactor: 0.8,
                                        fontWeightDelta: 3,
                                        color: Colors.black),
                                ),
                                TextSpan(text: "\n(Cash)",
                                  style: AppConstants.bodyFont.apply(
                                      fontSizeFactor: 0.6,
                                      fontWeightDelta: 1,
                                      color: Colors.black),
                                ),
                              ]
                            ),
                            textAlign: TextAlign.center,
                          ),
                          trailing: Text(
                            "\u20B9 70",
                            style: AppConstants.headingFont.apply(fontSizeFactor: 1.4),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
