import 'package:flutter/material.dart';
import 'package:foomart/Components/AddEditAddess.dart';
import 'package:foomart/Components/SortFilter.dart';
import 'package:foomart/Constants.dart';

import 'package:foomart/Components/LocationCheck.dart';
import 'package:foomart/account_page.dart';
import 'package:foomart/address_page.dart';
import 'package:foomart/dashboard_second.dart';
import 'package:foomart/favrouties.dart';
import 'package:foomart/order_history.dart';
import 'package:foomart/product_page.dart';
import 'package:foomart/dashboard.dart';
import 'package:foomart/login.dart';

import 'package:foomart/order_review.dart';
import 'package:foomart/register.dart';
import 'package:foomart/view_order.dart';

import 'Components/AccountEdit.dart';
import 'checkout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: AppConstants.createMaterialColor(AppConstants.appGreen),
        backgroundColor: Colors.white,
      ),
      home: Login(),
      routes: {
        '/login':(context) => Login(),
        '/register':(context) =>Register(),
        '/check_location':(context) => LocationCheck(),
        '/home':(context) => Dashboard(),
        '/home_2':(context) => DashboardSecond(),
        '/product_page':(context) => ProductPage(),
        '/checkout':(context) => Checkout(),
        '/confirm_order':(context) => OrderReview(),
        '/view_order':(context) => ViewOrder(),
        '/accounts_page':(context) => AccountsPage(),
        '/address_page': (context)=> AddressPage(),
        '/add_edit_address':(context) => AddEditAddress(),
        '/account_edit':(context)=> AccountEdit(),
        '/favourites':(context)=>Favourites(),
        '/order_history':(context)=>OrderHistory(),

      },
    );
  }
}
