import 'package:flutter/material.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';
import 'package:foomart/Constants.dart';

class AddressPage extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "Address Book",
          style: AppConstants.bodyFont
              .apply(color: Colors.black, fontSizeFactor: 0.8),
        ),
        titleSpacing: 0,
      ),
      body: SafeArea(
        child: Container(
          height: _size.height,
            child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [ AddressCard(addressType: "Home", isDefault: true),...List.generate(2, (index) => AddressCard(addressType: "Office", isDefault: false))],
              ),
            ),
            SizedBox(height: 20,),
            OutlinedButton(
              child: Text("Add New Address", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: AppConstants.appGreen),),
              style: TextButton.styleFrom(
                side: BorderSide(
                    color: AppConstants.appGreen),
              ),
              onPressed: () => Navigator.pushNamed(context, '/add_edit_address'),
            ),
            SizedBox(height: 20,),
          ],
        )),
      ),
      bottomNavigationBar: CustomBottomNavigation(),
    );
  }
  Widget AddressCard({String addressType, String address, bool isDefault}){
    return Card(
      borderOnForeground: false,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 5,
      margin: EdgeInsets.all(12),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical:8.0, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "$addressType",
              style: AppConstants.headingFont,
            ),
            SizedBox(height: 20.0,),
            Row(
              children: [
                Flexible(
                  flex: 2,
                  child: RichText(
                    text: TextSpan(
                        text:
                        "1/1/B, Maulana Abdul Kalam Road,Khirod Chandra Ghosh Lane, Babudanga, Pilkhana, Mali Panchghara, Howrah, West Bengal 711101",
                        style: AppConstants.bodyFont.apply(
                            color: Colors.black,
                            heightFactor: 1.2,
                            fontSizeFactor: 0.8)),
                  ),
                ),
                Flexible(
                    flex: 1,
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            width: double.infinity,
                            child: TextButton(
                              child: Text("Set as default", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: !isDefault?AppConstants.appGreen:Colors.white),),
                              style: TextButton.styleFrom(
                                backgroundColor:
                                isDefault?AppConstants.appGreen:AppConstants.appGreen.withOpacity(0.2),
                              ),
                              onPressed: (){},
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            child: OutlinedButton(
                              child: Text("Edit", style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, color: AppConstants.appGreen),),
                              style: TextButton.styleFrom(
                                side: BorderSide(
                                    color: AppConstants.appGreen),
                              ),
                              onPressed: (){Navigator.pushNamed(context, '/add_edit_address');},
                            ),
                          )
                        ],
                      ),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
