import 'package:flutter/material.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';
import 'package:foomart/Components/ResturantCardVertical.dart';
import 'package:foomart/Constants.dart';

import 'Components/SearchAndLocationAppBar.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return WillPopScope(
      onWillPop: () async{
        _showToast(context);
        return false;
      },
      child: Scaffold(
        backgroundColor: Color(0xFFF9F9F9),
        body: SafeArea(
          child: Container(
      height: _size.height,
      width: _size.width,
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SearchAndLocationAppBar(),
              SizedBox(
                height: 20,
              ),
              Placeholder(
                fallbackHeight: _size.height / 2 * _ratio,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Near by Resturants",
                style: AppConstants.bodyFont
                    .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
              ),
              SizedBox(
                height: 10.0,
              ),
              Flexible(
                  child: ListView.builder(
                      itemCount: 10,
                      itemBuilder: (_, currentCount) => InkWell(
                            child: RestaurantCardVertical(),
                            onTap: () {
                              Navigator.pushNamed(context, '/product_page');
                            },
                          ))),
            ]),
          ),
        ),
        bottomNavigationBar: CustomBottomNavigation(),
      ),
    );
  }
  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Press back twice to exit'),
        // action: SnackBarAction(label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
