import 'package:flutter/material.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';
import 'package:foomart/Components/CustomListTile.dart';
import 'package:foomart/Components/SearchAndLocationAppBar.dart';
import 'package:foomart/Constants.dart';
import 'package:google_fonts/google_fonts.dart';

class AccountsPage extends StatefulWidget {
  @override
  _AccountsPageState createState() => _AccountsPageState();
}

class _AccountsPageState extends State<AccountsPage> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return Scaffold(
      backgroundColor: AppConstants.appGreen,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 3, vertical: 4),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.85),
                    borderRadius: BorderRadius.circular(5.0)),
                child: Row(
                  children: [
                    Icon(
                      Icons.location_pin,
                      color: AppConstants.appGreen,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "MG Road",
                      style: GoogleFonts.josefinSans(
                        fontSize: 16,
                        color: AppConstants.appGreen,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Spacer(),
                    IconButton(
                        icon: Icon(
                          Icons.search,
                          color: AppConstants.appGreen,
                        ),
                        onPressed: () {})
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Flexible(
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10000),
                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Image.network(
                        "https://picsum.photos/200",
                        fit: BoxFit.cover,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          print(loadingProgress);
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Flexible(
                    child: RichText(
                      text: TextSpan(
                          text: "Sayed Akhtar".toUpperCase(),
                          children: [
                            TextSpan(text: "\n"),
                            TextSpan(text: "+91 7602121828"),
                            TextSpan(text: "\n"),
                            TextSpan(text: "Sayed@gmail.com")
                          ],
                          style: AppConstants.bodyFont.apply(
                              color: Colors.white,
                              fontSizeFactor: 0.7,
                              heightFactor: 1.5)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 30, left: 10, right: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0), topRight: Radius.circular(50.0))
                  ),
                  child: ListView(
                    children: [
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Account Settings")),
                        trailing: InkWell(onTap: (){Navigator.pushNamed(context, '/account_edit');},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Address Book")),
                        trailing: InkWell(onTap: (){Navigator.pushNamed(context, '/address_page');},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Order History")),
                        trailing: InkWell(onTap: (){},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Payments & Refunds")),
                        trailing: InkWell(onTap: (){},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Favourites")),
                        trailing: InkWell(onTap: (){},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                      CustomListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Container(padding:EdgeInsets.only(left: 40),child: CustomText("Offers")),
                        trailing: InkWell(onTap: (){},child: Icon(Icons.chevron_right),),
                      ),
                      Divider(color: AppConstants.textGrey,),
                    ],
                  ),
                ),
              )

            ],
          ),
        ),
      ),
      bottomNavigationBar: CustomBottomNavigation(),
    );
  }
  Widget CustomText(String text){
    return Text(
      text,
      style: AppConstants.bodyFont.apply(fontSizeFactor: 0.7, fontWeightDelta: 2),
    );
  }
}
