import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foomart/Components/CustomBottomNavigation.dart';
import 'package:foomart/Components/ResturantCardVertical.dart';
import 'package:foomart/Components/ResturantCardWithCounter.dart';
import 'package:foomart/Constants.dart';

import 'Components/ResturantCardHorizontalWithCounter.dart';
import 'Components/SearchAndLocationAppBar.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    double _ratio = AppConstants.appReponsiveRatio;
    return SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF9F9F9),
          body: Container(
           
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SearchAndLocationAppBar(),
                    SizedBox(
                      height: 20,
                    ),
                    Placeholder(
                      fallbackHeight: _size.height / 2 * _ratio,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Text(
                          "Populars Menu",
                          style: AppConstants.bodyFont
                              .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                        ),
                        Spacer(),
                        SvgPicture.asset('assets/icons/grid.svg', height: 30, width:30,),
                        SizedBox(width: 10,),
                        SvgPicture.asset('assets/icons/view-list-button.svg', height: 30, width:30,fit: BoxFit.contain,),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 300,
                      width: _size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          RestaurantCardHorizontalWithCounter(),
                          RestaurantCardHorizontalWithCounter(),
                          RestaurantCardHorizontalWithCounter(),

                        ],
                      ),
                    ),
                    Text(
                      "Starter Non Veg",
                      style: AppConstants.bodyFont
                          .apply(fontWeightDelta: 2, color: Color(0xff3E3F68)),
                    ),
                    SizedBox(height: 10.0,),
                    RestaurantCardWithCounter(),
                    RestaurantCardWithCounter(),
                    RestaurantCardWithCounter(),
                  ]),
            ),
          ),
          bottomNavigationBar: CustomBottomNavigation(),
        ));
  }
}
